const create = (source) => {
    return {
        "google_id": source.googleId,
        "first_name": source.firstName,
        "last_name": source.lastName,
        "email": source.email,
        "image": source.image,
        "is_admin": source.isAdmin || false,
        "allow_access": source.allowAccess || false,
        "templates": source.templates || [],
        "orders": source.orders || []
    }
}

const map = (record) => {
    return {
        id: record.getId(),
        googleId: record.get('google_id'),
        firstName: record.get('first_name'),
        lastName: record.get('last_name'),
        email: record.get('email'),
        image: record.get('image'),
        allowAccess: record.get('allow_access') || false,
        isAdmin: record.get('is_admin') || false,
        templates: record.get('templates') || [],
        orders: record.get('orders') || [],
        created: record.get('created_date')
    }
}

module.exports = {
    create,
    map
}