const create = (source) => {
    return {
        "name": source.name,
        "address": source.address,
        "phone": source.phone,
        "email": source.email,
        "image": source.image,
        "contact_name": source.contact,
        "orders": source.orders || []        
    }
}

const map = (record) => {
    return {
        id: record.getId(),
        name: record.get('name'),
        address: record.get('address'),
        phone: record.get('phone'),
        email: record.get('email'),
        contact: record.get('contact_name'),
        orders: record.get('orders') || [],        
        image: record.get('image') ? record.get('image')[0]:null,        
        created: record.get("created_date")
    }
}

module.exports = {
    create,
    map
}