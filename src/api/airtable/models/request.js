const create = (source) => {
    return {
        "subject": source.subject,
        "body": source.body,
        "items": source.items,
        "vendor": source.vendor,        
        "sent_by": source.sentBy,
        "template": source.template,
        "order": source.order        
    }
}

const map = (record) => {
    return {
        id: record.getId(),
        subject: record.get("subject"),
        body: record.get("body"),
        items: record.get("items"),
        vendor: record.get("vendor") || [],
        sentBy: record.get("sent_by") || [],
        template: record.get("template") || [],
        order: record.get("order") || [],
        createdDate: record.get("created_date")
    }
}
module.exports = {
    create,
    map
}