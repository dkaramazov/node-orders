const create = (source) => {
    return {
        "name": source.name,
        "unit_cost": source.unitCost,
        "description": source.description,
        "orders": source.orders            
    }
}

const map = (record) => {
    return {
        id: record.getId(),
        name: record.get("name"),
        unitCost: record.get("unit_cost"),
        description: record.get("description"),
        orders: record.get("orders")        
    }
}

module.exports = {
    create,
    map
}