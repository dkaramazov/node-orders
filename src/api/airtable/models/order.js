const create = (source) => {
    return {
        "vendor_id": [source.vendor.id] || [],
        "items": source.items || [],
        "location_id": [source.locationId] || [],
        "location_display_name": source.locationDisplayName,
        "additional_notes": source.additionalNotes || '',
        "delivered": source.delivered || null,
        "paid": source.paid || null,
        "received_by": source.receivedBy || null,
        "requests": source.requests || []
    }
}

const map = (record) => {
    return {
        id: record.getId(),
        vendor: {
            id: record.get("vendor_id"),
            displayName: record.get("vendor_display_name")
        },        
        items: record.get("items"),
        location: record.get("location_id"),
        locationDisplayName: record.get("location_display_name"),
        additionalNotes: record.get("additional_notes"),
        delivered: record.get("delivered") || false,
        paid: record.get("paid") || false,                    
        receivedBy: record.get("received_by"),                    
        requests: record.get("requests") || [],
        created: record.get("created_date")
    }
}

module.exports = {
    create,
    map
}