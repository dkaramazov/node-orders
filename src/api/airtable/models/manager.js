const create = (source) => {
    return {
        "display_name": source.displayName,
        "user": [source.user],
        "locations": source.locations || [],
        "mobile": source.mobile,        
        "created_date": source.created         
    }
}

const map = (record) => {
    return {
        id: record.getId(),
        displayName: record.get("display_name"),
        user: record.get("user") || [],
        locations: record.get("locations") || [],
        mobile: record.get("mobile") || [],
        created: record.get("created_date") || []        
    }
}
module.exports = {
    create,
    map
}