const create = (source) => {
    return {
        "name": source.name,
        "address": source.address,
        "phone": source.phone,
        "orders": source.orders        
    }
}

const map = (record) => {
    return {
        id: record.getId(),
        name: record.get("name"),
        address: record.get("address"),
        phone: record.get("phone"),
        orders: record.get("orders")        
    }
}

module.exports = {
    create,
    map
}