const create = (source) => {
    return {
        "title": source.title,
        "vendor_id": [source.vendorId] || [],
        "subject": source.subject,
        "body": source.body,
        "notes": source.notes,
        "created_by": [source.user] || []
    }
}

const map = (record) => {
    return {
        id: record.getId(),
        title: record.get('title'),
        vendorId: record.get('vendor_id'),
        subject: record.get('subject'),
        body: record.get('body'),
        notes: record.get('notes'),
        user: record.get('created_by'),
        requests: record.get('requests'),
        created: record.get('created_date')
    }
}
module.exports = {
    create,
    map
}