var Airtable = require('airtable');
const airtable = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY }); 
const base = airtable.base(process.env.AIRTABLE_BASE_KEY);

const Items = base('items');
const Locations = base('locations');
const Managers = base('managers');
const Orders = base('orders');
const Requests = base('requests');
const Templates = base('templates');
const Users = base('users');
const Vendors = base('vendors');

const Item = require('./models/item');
const Location = require('./models/location');
const Manager = require('./models/manager');
const Order = require('./models/order');
const Request = require('./models/request');
const Template = require('./models/template');
const User = require('./models/user');
const Vendor = require('./models/vendor');

module.exports = {
    bases: {
        Items,
        Locations,
        Managers,
        Orders,
        Requests,
        Templates,
        Users,
        Vendors
    },
    models: {
        Item,
        Location,
        Manager,
        Order,
        Request,
        Template,
        User,
        Vendor
    }
}