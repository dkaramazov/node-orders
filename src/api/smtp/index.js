var nodemailer = require('nodemailer');
var Email = require('email-templates');

var transport = nodemailer.createTransport({
    service: 'gmail',
    host: process.env.EMAIL_USERNAME,
    auth: {
      type: 'OAuth2',
      user: process.env.EMAIL_USERNAME,
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      refreshToken: process.env.GOOGLE_REFRESH_TOKEN,
      accessToken: process.env.GOOGLE_ACCESS_TOKEN,
      expires: 3600
    }
});

const emailClient = new Email({
    send: true,
    message: {
        from: process.env.EMAIL_USERNAME
    },
    transport,
    views: {
        root: 'src/api/smtp/emails',
        options: {
            extension: 'handlebars'
        }
    }
});

module.exports = emailClient;