const plivo = require('plivo');
const smsClient = new plivo.Client(process.env.PLIVO_AUTH_ID, process.env.PLIVO_AUTH_TOKEN);

module.exports = smsClient;