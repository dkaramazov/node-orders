const smsClient = require('./sms');
const emailClient = require('./smtp');
const { bases, models } = require('./airtable');

module.exports = {
    smsClient,
    emailClient,
    models,
    bases
}