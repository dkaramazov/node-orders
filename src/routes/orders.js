const express = require("express");
const router = express.Router();
const { sendEmail } = require("../controllers/emailController");
const { sendText } = require("../controllers/textingController");
const { checkAuthenticated } = require("../helpers/auth");
const { Orders, Templates } = require("../api").bases;
const { Order, Template } = require("../api").models;
const orderController = require("../controllers/orderController");
const requestController = require("../controllers/requestController");
const vendorController = require("../controllers/vendorController");
const locationController = require("../controllers/locationController");
const templateController = require("../controllers/templateController");
const managerController = require("../controllers/managerController");

router.get("/", checkAuthenticated, async (req, res) => {
  var vendors = await vendorController.get({});
  var orders = await orderController.get({
    view: "completed",
    sort: [{ field: "created_date", direction: "desc" }]
  });
  if (orders && orders.length) {
    vendors.forEach(function(vendor) {
      if (vendor.orders) {
        vendor.orders = vendor.orders.map(function(id) {
          return orders.find(function(order) {
            return order.id === id && order.paid && order.delivered;
          });
        });
      }
    });
  } else {
    vendors.forEach(function(vendor) {
      vendor.orders = [];
    });
  }
  vendors.forEach(function(vendor) {
    vendor.orders = vendor.orders.filter(function(order) {
      return order || null;
    });
  });
  res.render("index/dashboard", { vendors, status: "Completed" });
});

router.get("/status/:id", async (req, res) => {
  var vendors = await vendorController.get({});
  var orders = await orderController.get({
    view: req.params.id || 'pending',
    sort: [{ field: "created_date", direction: "desc" }]
  });
  if (orders && orders.length) {
    vendors.forEach(function(vendor) {
      if (vendor.orders) {
        vendor.orders = vendor.orders.map(function(id) {
          return orders.find(function(order) {
            return order.id === id && !order.received;
          });
        });
      }
    });
  } else {
    vendors.forEach(function(vendor) {
      vendor.orders = [];
    });
  }
  vendors.forEach(function(vendor) {
    vendor.orders = vendor.orders.filter(function(order) {
      return order || null;
    });
  });
  res.render(`index/${req.user.isAdmin ? 'dashboard':'external'}`, { vendors, status: req.params.id || 'Pending' });
  // res.render("index/external", { vendors, status: "Pending", layout: "add" });
});

router.get("/add", async (req, res) => {
  const vendors = await vendorController.get({});
  const locations = await locationController.get({});
  res.render("orders/add", { vendors, locations });
  // res.render("orders/add", { layout: "add" });
});

router.post("/add", async (req, res) => {
  try {
    var order = await orderController.create({
      vendor: { id: req.body.vendor },
      items: req.body.items,
      locationId: req.body.location,
      additionalNotes: req.body.additionalNotes,
      requests: []
    });
    var templates = await templateController.get({});
    templates = templates.filter(function(template) {
      template.orderId = order.id;
      return template.vendorId.indexOf(req.body.vendor) > -1;
    });
    const vendor = await vendorController.getById(req.body.vendor);
    res.render(`templates/choose`, { vendor, templates });
  } catch (err) {
    console.log(err);
    return;
  }
});

router.get("/request/:orderId/:templateId", checkAuthenticated, async (req, res) => {
    const template = await templateController.getById(req.params.templateId);
    const order = await orderController.getById(req.params.orderId);
    const vendor = await vendorController.getById(order.vendor.id);
    res.render("orders/request", { order, template, vendor });
  }
);

router.get("/request/:orderId/", checkAuthenticated, async (req, res) => {
  var templates = await templateController.get({});
  var order = await orderController.getById(req.params.orderId);
  var vendor = await vendorController.getById(order.vendor.id[0]);
  templates = templates.filter(function(template) {
    template.orderId = req.params.orderId;
    return template.vendorId.indexOf(order.vendor.id[0]) > -1;
  });
  res.render(`templates/choose`, { vendor, templates });
});

router.post("/request/:id", checkAuthenticated, async (req, res) => {
  var order = await orderController.getById(req.params.id);
  const vendor = await vendorController.getById(order.vendor.id);
  var mailOptions = {
    from: req.user.email,
    to: vendor.email,
    template: "request",
    subject: req.body.subject,
    body: req.body.body
  };
  try {
    const response = await sendEmail(mailOptions);
    // if (process.env.NODE_ENV !== "testing") {
    var request = await requestController.create({
      subject: req.body.subject,
      body: req.body.body,
      items: req.body.items,
      order: [order.id],      
      vendor: vendor.id,
      sentBy: [req.user.id]
    });
    order.requests.push(request.id);
    order = await orderController.update(order.id, {
      requests: order.requests
    });
    // }
    res.redirect("/dashboard");
  } catch (err) {
    console.log(err);
    res.redirect("/dashboard");
  }
});

router.get("/:id", checkAuthenticated, async (req, res) => {
  try {
    var order = await orderController.getById(req.params.id);
    order.vendor = await vendorController.getById(order.vendor.id);
    var requests = [];
    if (order.requests && order.requests.length) {
      for (var i = 0; i < order.requests.length; i++) {
        requests.push(await requestController.getById(order.requests[i]));
      }
    }
  } catch (err) {
    console.log(err);
  }
  res.render("orders/show", { order, requests });
});

router.get("/vendor/:id", checkAuthenticated, async (req, res) => {
  try {
    var vendor = await vendorController.getById(req.params.id);
    var orders = await orderController.get({
      formula: `IF({vendor_id} = ${vendor.id}, true, false)`,
      sort: [{ field: "created_date", direction: "desc" }]
    });
    if (vendor.orders) {
      vendor.orders = vendor.orders
        .map(function(id) {
          return orders.find(function(order) {
            return order.id === id && !order.received;
          });
        })
        .filter(function(order) {
          return order || null;
        });
    }
  } catch (err) {
    console.log(err);
  }
  res.render("index/dashboard", { vendors: [vendor], status: vendor.name });
});

router.put("/paid/:id", checkAuthenticated, async (req, res) => {
  try {
    const order = await orderController.toggle(req.params.id, "paid");
    res.redirect("/dashboard");
  } catch (err) {
    console.log(err);
    res.redirect("/dashboard");
  }
});
router.put("/delivered/:id", checkAuthenticated, async (req, res) => {
  try {
    const order = await orderController.toggle(req.params.id, "delivered");
    const location = await locationController.getById(order.location[0] || null);
    let managers = await managerController.get({});
    managers = managers.filter(function(manager){
      return manager.locations.indexOf(order.location[0]) > -1;
    });    
    if(!req.user.isAdmin){
      // send text with /orders/show/id to each matched by location
      var numbers = managers.reduce(function(acc, manager){
        acc.push(manager.mobile);
        return acc;
      }, []);    
      const response = await sendText(numbers, 
        `Order received from ${order.vendor.displayName} at ${location.name}!      
        ${process.env.DEPLOYMENT_URL}/orders/${order.id}`);
        res.render("index/success", { message: `Manager(s) notified of delivery.`});
    } else {
      res.redirect("/dashboard");
    }
  } catch (err) {
    console.log(err);
    res.redirect("/dashboard");
  }
});
router.put("/completed/:id", checkAuthenticated, async (req, res) => {
  try {
    const order = await orderController.toggle(req.params.id, "completed");
    res.redirect("/dashboard");
  } catch (err) {
    console.log(err);
    res.redirect("/dashboard");
  }
});

router.put("/:id", checkAuthenticated, async (req, res) => {
  const order = await orderController.toggle(req.params.id);
  res.redirect("/dashboard");
});

module.exports = router;
