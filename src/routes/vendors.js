const express = require("express");
const router = express.Router();
const { checkAuthenticated, checkGuest } = require("../helpers/auth");
const orderController = require("../controllers/orderController");
const vendorController = require("../controllers/vendorController");
const axios = require('axios');

router.get("/", checkAuthenticated, async (req, res) => {
  var vendors = await vendorController.get({});
  res.render("vendors/index", { vendors });
});

router.get("/add", checkAuthenticated, async (req, res) => {
  res.render("vendors/add", {});
});

router.get("/edit/:id", checkAuthenticated, async (req, res) => {
  const vendor = await vendorController.getById(req.params.id);
  res.render("vendors/edit", { vendor });
});

router.get("/show/:id", async (req, res) => {  
  const vendor = await vendorController.getById(req.params.id);
  res.render("vendors/show", { vendor });
});

router.delete("/:id", async (req, res) => {
  const response = await vendorController.removeById(req.params.id);
  res.redirect('/vendors');
});

router.put("/:id", checkAuthenticated, async (req, res) => {  
  var vendor = await vendorController.updateById(req.params.id, {
    name: req.body.name,
    address: req.body.name,
    phone: req.body.phone,
    email: req.body.email,    
    contact_name: req.body.contact
  });
  res.render(`vendors/show`, { vendor });
});

router.post("/add", checkAuthenticated, async (req, res) => {
  const vendor = await vendorController.create({
    name: req.body.name,
    address: req.body.name,
    phone: req.body.phone,
    email: req.body.email,
    image: req.body.image,
    contact: req.body.contact,
    orders: null
  });
  res.render("vendors/show", { vendor });
});

router.get("/about", (req, res) => {
  res.render("index/about");
});

module.exports = router;
