const express = require("express");
const router = express.Router();
const { Managers, Vendors } = require("../api").bases;
const { Template, Vendor, Manager } = require("../api").models;
const vendorController = require("../controllers/vendorController");
const templateController = require("../controllers/templateController");
const locationController = require("../controllers/locationController");
const managerController = require("../controllers/managerController");
const userController = require("../controllers/userController");
const { checkAuthenticated } = require("../helpers/auth");

router.get("/", checkAuthenticated, async (req, res) => {
  const managers = await managerController.get({}); 
  for(var i = 0; i < managers.length; i++) {
      managers[i].user = await userController.getById(managers[i].user[0]);            
      for(var j = 0; j < managers[i].locations.length; j++){
        managers[i].locations[j] = await locationController.getById(managers[i].locations[j]);          
      }
  }
  res.render("managers/index", { managers });
});

router.get("/add", checkAuthenticated, async (req, res) => {
  const users = await userController.get({});
  const locations = await locationController.get({});
  res.render("managers/add", { users, locations });
});

router.get("/:id", checkAuthenticated, async (req, res) => {
  var templates = await templateController.get({});
  var vendor = await vendorController.getById(req.params.id);
  templates = templates.filter(template => {
    return template.vendorId.indexOf(req.params.id) > -1;
  });
  res.render("templates/index", { templates, message: vendor.name });
});

router.get("/edit/:id", checkAuthenticated, async (req, res) => {
  let manager = await managerController.getById(req.params.id);  
  manager.user = await userController.getById(manager.user);
  for(var i = 0; i < manager.locations.length; i++){
    manager.locations[i] = await locationController.getById(manager.locations[i]);
  }
  res.render("managers/edit", { manager });
});

router.put("/:id", async (req, res) => {
  await managerController.updateById(req.params.id, {
    display_name: req.body.displayName,
    mobile: req.body.mobile
  });
  res.redirect("/managers");
});

router.delete("/:id", async (req, res) => {
  const response = templateController.removeById(req.params.id);
  res.redirect("/templates");
});

router.post("/add", checkAuthenticated, async (req, res) => {
  try {
      const manager = await managerController.create({
        displayName: req.body.displayName,
        user: req.body.user,
        locations: req.body.locations,
        mobile: req.body.mobile    
      });
      if(manager){
          res.render('managers/show', {manager});
      } else {
          throw err;
      }
  } catch(err){
      console.log(err);
      res.redirect('managers/index', { message: 'Failed to add manager'} );
  }
});

router.get("/show/:id", async (req, res) => {
  const template = await templateController.getById(req.params.id);
  const vendor = await vendorController.getById(template.vendorId);
  res.render("templates/show", { template, vendor });
});

module.exports = router;
