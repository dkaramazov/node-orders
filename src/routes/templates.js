const express = require("express");
const router = express.Router();
const { Templates, Vendors } = require("../api").bases;
const { Template, Vendor } = require("../api").models;
const vendorController = require("../controllers/vendorController");
const templateController = require("../controllers/templateController");
const { checkAuthenticated } = require("../helpers/auth");

router.get("/", checkAuthenticated, async (req, res) => {
  const templates = await templateController.get({});
  res.render("templates/index", { templates });
});

router.get("/add", checkAuthenticated, async (req, res) => {
  const vendors = await vendorController.get({});  
  res.render("templates/add", { vendors });
});

router.get("/choose/:id", checkAuthenticated, async (req, res) => {
  const templates = await templateController.get({});
  templates.forEach(function(template){
    template.orderId = req.params.id;
  });  
  res.render("templates/choose", { templates });
});

router.get("/my", checkAuthenticated, async (req, res) => {
    const templates = await templateController.getByUserId(req.user.id);
    res.render("templates/index", { templates });
});

router.get("/:id", checkAuthenticated, async (req, res) => {
  var templates = await templateController.get({});
  var vendor = await vendorController.getById(req.params.id);
  templates = templates.filter(template => {
      return template.vendorId.indexOf(req.params.id) > -1;
  });    
  res.render("templates/index", { templates, message: vendor.name });
});

router.get("/edit/:id", checkAuthenticated, async (req, res) => {
  const template = await templateController.getById(req.params.id);
  const vendor = await vendorController.getById(template.vendorId);
  res.render("templates/edit", { template, vendor });
});

router.put("/:id", async (req, res) => {
  const template = Template.create({
    title: req.body.title,
    subject: req.body.subject,
    vendorId: req.body.vendor,
    notes: req.body.notes,
    body: req.body.body,
    user: req.user.id
  });
  await templateController.updateById(req.params.id, template);
  res.redirect('/templates');
});

router.delete("/:id", async (req, res) => {
  const response = templateController.removeById(req.params.id);
  res.redirect('/templates');  
});



router.post("/", checkAuthenticated, (req, res) => {
  const template = Template.create({
    title: req.body.title,
    subject: req.body.subject,
    vendorId: req.body.vendor,
    notes: req.body.notes,
    body: req.body.body,
    user: req.user.id
  });
  Templates.create(template, function(err, record) {
    if (err) {
      console.error(err);
      return;
    }
    res.redirect("/templates");
  });
});

router.get("/show/:id", async (req, res) => {
  const template = await templateController.getById(req.params.id);
  const vendor = await vendorController.getById(template.vendorId);
  res.render("templates/show", { template, vendor });
});

module.exports = router;
