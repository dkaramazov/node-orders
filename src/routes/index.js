const express = require("express");
const router = express.Router();
const { checkAuthenticated, checkGuest } = require("../helpers/auth");
const orderController = require("../controllers/orderController");
const vendorController = require("../controllers/vendorController");

router.get("/", checkGuest, (req, res) => {
  res.render("index/welcome");
});

router.get("/dashboard", checkAuthenticated, async (req, res) => {
  var vendors = await vendorController.get({});
  var orders = await orderController.get({
    view: `${req.user.isAdmin ? 'admin':'pending'}`,
    sort: [{ field: "created_date", direction: "desc" }]
  });
  vendors.forEach(function(vendor){
    if(vendor.orders){
      vendor.orders = vendor.orders.map(function(id){
        return orders.find(function(order){
          return order.id === id;
        });
      });
    }
  });  
  vendors.forEach(function(vendor){
    if(vendor.orders){
      vendor.orders = vendor.orders.filter(function(order){
        return order || null;
      });
    }
  });  
  res.render(`index/${req.user.isAdmin ? 'dashboard':'external'}`, { vendors, status: `${req.user.isAdmin ? '':'Pending'}` });
});

router.get("/about", (req, res) => {
  res.render("index/about");
});

module.exports = router;
