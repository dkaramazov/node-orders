const express = require("express");
const router = express.Router();
const { checkAuthenticated, checkAdmin } = require("../helpers/auth");
const {
  getAll,
  toggleAccess,
  toggleAdmin,
  upsertMobile,
  getMobile
} = require("../controllers/userController");

router.get("/", checkAdmin, checkAuthenticated, getAll);

router.get("/mobile/:id", checkAdmin, checkAuthenticated, getMobile);

router.put("/mobile/:id", checkAdmin, checkAuthenticated, upsertMobile);

router.put("/access/:id", checkAdmin, checkAuthenticated, (req, res) => {
  toggleAccess(req.params.id, user => {
    res.render("admin/users", {
      message: `Access changed for ${user.firstName} ${user.lastName}`
    });
  });
});

router.put("/role/:id", checkAdmin, checkAuthenticated, (req, res) => {
  toggleAdmin(req.params.id, user => {
    res.render("admin/users", {
      message: `Role changed for ${user.firstName} ${user.lastName}`
    });
  });
});
module.exports = router;
