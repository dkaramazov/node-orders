const moment = require("moment");
module.exports = {
  truncate: function(str, len) {
    if (str.length > len && str.length > 0) {
      var new_str = str + " ";
      new_str = str.substr(0, len);
      new_str = str.substr(0, new_str.lastIndexOf(" "));
      new_str = new_str.length > 0 ? new_str : str.substr(0, len);
      return new_str + "...";
    }
    return str;
  },
  stripTags: function(input) {
    return input.replace(/<(?:.|\n)*?>/gm, "");
  },
  formatDate: function(date, format) {
    return moment(date).format(format);
  },
  select: function(selected, options) {
    return options
      .fn(this)
      .replace(new RegExp(' value="' + selected + '"'), '$&selected="selected"')
      .replace(
        new RegExp(">" + selected + "</option>"),
        'selected="selected"$&'
      );
  },
  managerEditIcon: function(user, loggedUser, managerId, floating = true) {
    // if (user == loggedUser) {
      if (floating) {
        return `<a href="/managers/edit/${managerId}" class="btn-floating halfway-fab red"><i class="fa fa-pencil"></i></a>`;
      } else {
        return `<a href="/managers/edit/${managerId}"><i class="fa fa-pencil"></i></a>`;
      }
    // } else {
    //   return "";
    // }
  },
  templateEditIcon: function(user, loggedUser, templateId, floating = true) {
    if (floating) {
      return `<a href="/templates/edit/${templateId}" class="btn-floating halfway-fab red"><i class="fa fa-pencil"></i></a>`;
    } else {
      return `<a href="/templates/edit/${templateId}"><i class="fa fa-pencil"></i></a>`;
    }
  },
  vendorEditIcon: function(vendorId, floating = true) {
    if (floating) {
      return `<a href="/vendors/edit/${vendorId}" class="btn-floating halfway-fab red"><i class="fa fa-pencil"></i></a>`;
    } else {
      return `<a href="/vendors/edit/${vendorId}"><i class="fa fa-pencil"></i></a>`;
    }
  }
};
