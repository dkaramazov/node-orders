const { Templates } = require("../api").bases;
const { Template } = require("../api").models;

const get = async function(params){
    const templates = await Templates.select(params).all();
    return templates.map(function(template){
        return Template.map(template);
    });         
}

const getById = async function(id) {    
    try {
        let record = await Templates.find(id);        
        return Template.map(record);
    } catch(err){
        console.log(err);
        return null;
    }
}

const updateById = async function(id, template) {    
    try {
        let record = await Templates.replace(id, template);        
        return Template.map(record);
    } catch(err){
        console.log(err);
        return null;
    }
}
const getByUserId = async function(id) {    
    try {
        let records = await Templates.select({}).all();  
        return records.map(function(record){
            return Template.map(record);
        }).filter(function(template){
            return template.user && template.user.length > 0
            ? template.user.indexOf(id) > -1
            : false;
        });        
    } catch(err){
        console.log(err);
        return null;
    }
}

const removeById = async function(id){
    try {
      const response = await Templates.destroy(id);
      return response;
    } catch (err){
      console.log(err);
      return null;
    }
  }

module.exports = {
  getById,
  get,
  updateById,
  getByUserId,
  removeById
};