const { Users } = require("../api").bases;
const { User } = require("../api").models;

const getAll = async (req, res) => {
  try {
    var users = await Users.select({}).all();
    users = users.map(user => {
      return User.map(user);
    });
    res.render("admin/users", { users });
  } catch (err) {
    if (err) return res.send("An error occurred.");
  }
};

const get = async params => {
  try {
    var users = await Users.select(params).all();
    users = users.map(user => {
      return User.map(user);
    });
    return users;
  } catch (err) {
    if (err) return null;
  }
};

const getById = async id => {
  try {
    var user = await Users.find(id);
    return User.map(user);
  } catch (err) {
    if (err) return null;
  }
};

const getMobile = (req, res) => {
  Users.find(req.params.id, function(err, record) {
    if (err) {
      console.error(err);
      return;
    }
    const user = User.map(record);
    res.render("admin/mobile", { id: user.id, mobile: user.mobile });
  });
};

const upsertMobile = (req, res) => {
  Users.find(req.params.id, function(err, record) {
    if (err) {
      console.error(err);
      return;
    }
    Users.update(
      req.params.id,
      {
        mobile_number: req.body.mobile
      },
      function(err, record) {
        if (err) {
          console.error(err);
          return;
        }
        res.redirect("/admin");
      }
    );
  });
};

const createUser = (req, res) => {
  Users.create(User.create(req.body), function(err, record) {
    if (err) {
      console.error(err);
      return;
    }
    res.send(record._rawJson);
  });
};

const toggleAccess = (id, cb) => {
  Users.find(id, function(err, record) {
    if (err) {
      console.error(err);
      return;
    }
    Users.update(
      id,
      {
        allowAccess: !record._rawJson.fields.allowAccess
      },
      function(err, record) {
        if (err) {
          console.error(err);
          return;
        }
        cb(User.map(record));
      }
    );
  });
};

const toggleAdmin = (id, cb) => {
  Users.find(id, function(err, record) {
    if (err) {
      console.error(err);
      return;
    }
    Users.update(
      id,
      {
        isAdmin: !record.get("isAdmin")
      },
      function(err, record) {
        if (err) {
          console.error(err);
          return;
        }
        cb(User.map(record));
      }
    );
  });
};

module.exports = {
  toggleAdmin,
  toggleAccess,
  createUser,
  upsertMobile,
  getMobile,
  getById,
  getAll,
  get
};
