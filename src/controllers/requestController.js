const { Requests } = require("../api").bases;
const { Request } = require("../api").models;

const getById = async function(id) {
    try {
        const request = await Requests.find(id);                
        return Request.map(request);
    } catch(err){
        console.log(err)
        return null;
    }
}

const create = async function(request) {
    try {
      const record = await Requests.create(Request.create(request));
      return Request.map(record);
    } catch (err) {
      console.log(err);
      return;
    }
  };

module.exports = {
  getById,
  create
};