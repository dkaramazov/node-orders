const { Managers } = require("../api").bases;
const { Manager } = require("../api").models;

const get = async function(params) {
  const managers = await Managers.select(params).all();
  return managers.map(function(template) {
    return Manager.map(template);
  });
};

const getById = async function(id) {
  try {
    let record = await Managers.find(id);
    return Manager.map(record);
  } catch (err) {
    console.log(err);
    return null;
  }
};

const updateById = async function(id, manager) {
  try {
    let record = await Managers.update(id, manager);
    return Manager.map(record);
  } catch (err) {
    console.log(err);
    return null;
  }
};

const getByUserId = async function(id) {
  try {
    let records = await Managers.select({}).all();
    return records
      .map(function(record) {
        return Manager.map(record);
      })
      .filter(function(manager) {
        return manager.user && manager.user.length > 0
          ? manager.user.indexOf(id) > -1
          : false;
      });
  } catch (err) {
    console.log(err);
    return null;
  }
};

const removeById = async function(id) {
  try {
    const response = await Managers.destroy(id);
    return response;
  } catch (err) {
    console.log(err);
    return null;
  }
};

const create = async function(manager) {
    try {
      const record = await Managers.create(Manager.create(manager));
      return Manager.map(record);
    } catch (err) {
      console.log(err);
      return;
    }
  };

module.exports = {
  getById,
  get,
  updateById,
  getByUserId,
  removeById,
  create
};
