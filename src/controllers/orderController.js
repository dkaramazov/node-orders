const { Orders } = require("../api").bases;
const { Order } = require("../api").models;
const requestController = require("./requestController");

const get = async function(params) {
  const orders = await Orders.select(params).all();
  return orders.map(function(order) {
    return Order.map(order);
  });
};

const getById = async function(id) {
  try {
    let record = await Orders.find(id);
    return Order.map(record);
  } catch (err) {
    console.log(err);
    return null;
  }
};

const create = async function(order) {
  try {
    const record = await Orders.create(Order.create(order));
    return Order.map(record);
  } catch (err) {
    console.log(err);
    return;
  }
};

const update = async function(id, params) {
  try {
    const record = await Orders.update(id, params);
    return Order.map(record);
  } catch (err) {
    console.log(err);
    return;
  }
};

const toggle = async function(id, fieldName) {
  const order = await getById(id);
  var obj = {};
  obj[fieldName] = !order[fieldName];
  const record = await Orders.update(id, obj);
  return Order.map(record);
};

module.exports = {
  create,
  getById,
  get,
  toggle,
  update
};
