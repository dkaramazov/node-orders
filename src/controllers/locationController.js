const { Locations } = require("../api").bases;
const { Location } = require("../api").models;

const get = async function(params){
    const locations = await Locations.select(params).all();
    return locations.map(function(location){
        return Location.map(location);
    });         
}

const getById = async function(id) {    
    try {
        let record = await Locations.find(id);        
        return Location.map(record);
    } catch(err){
        console.log(err);
        return null;
    }
}

module.exports = {
  getById,
  get
};