const { emailClient } = require("../api");

const sendEmail = async mailOptions => {
  try {
    const response = await emailClient.send({
      template: mailOptions.template,
      message: {
        to: mailOptions.to
      },
      locals: {
        body: mailOptions.body,
        items: mailOptions.items,
        subject: mailOptions.subject
      }
    });
    return response;
  } catch (error) {
    return error;
  }
};

const createNotification = async function(order) {
//   const subject = `New inquiry from ${contact.name}`;
//   const body = `
//             <h3>${contact.name} + ${contact.fiance}</h3>
//             <p>Date: ${contact.date}</p>
//             <p>Email: ${contact.email}</p>
//             <p>Venue: ${contact.venue}</p>
//             <p>Coordinator/Planner: ${contact.coordinator}</p>
//             <p>Budget: ${contact.budget}</p>
//             <p>Quality: ${contact.quality}</p>
//             <p>Details: ${contact.details}</p>
//             <a href="${process.env.DEPLOYMENT_URL}/contacts/${
//     record._rawJson.id
//   }">REPLY TO ${contact.name}</a>
//             `;
//   var mailOptions = {
//     from: process.env.EMAIL_USERNAME,
//     to: process.env.EMAIL_USERNAME,
//     template: "notification",
//     subject: subject,
//     body: body
//   };
  return mailOptions;
};

module.exports = {
  sendEmail,
  createNotification
};
