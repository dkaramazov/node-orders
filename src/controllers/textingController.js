const { smsClient } = require("../api");

const sendText = async (numbers, message) => {
  try {
    let response;
    if(numbers && numbers.length){
        for(var i = 0; i < numbers.length; i++){
            response = await smsClient.messages.create(
              process.env.SOURCE_NUMBER,
              numbers[i],
              message
            );
        }
    }
    return response;
  } catch (error) {
    return error;
  }
};

module.exports = { sendText };
