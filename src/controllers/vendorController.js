const { Vendors } = require("../api").bases;
const { Vendor } = require("../api").models;

const get = async function(params) {
  const vendors = await Vendors.select(params).all();
  return vendors.map(function(vendor) {
    return Vendor.map(vendor);
  });
};

const getById = async function(id) {
  try {
    let record = await Vendors.find(id);
    return Vendor.map(record);
  } catch (err) {
    console.log(err);
    return null;
  }
};

const create = async function(order) {
  try {
    const record = await Vendors.create(Vendor.create(order));
    return Vendor.map(record);
  } catch (err) {
    console.log(err);
    return;
  }
};

const updateById = async function(id, vendor) {    
  try {
      let record = await Vendors.replace(id, vendor);        
      return Vendor.map(record);
  } catch(err){
      console.log(err);
      return null;
  }
}

const removeById = async function(id){
  try {
    const response = await Vendors.destroy(id);
    return response;
  } catch (err){
    console.log(err);
    return null;
  }
}

module.exports = {
  getById,
  get,
  create,
  updateById,
  removeById
};
